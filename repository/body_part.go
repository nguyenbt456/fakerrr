package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type BodyPartRepo struct {
	db *gorm.DB
}

func NewBodyPartRepository(db *gorm.DB) model.BodyPartRepository {
	return &BodyPartRepo{db}
}

func (r *BodyPartRepo) Create(bodyPart *model.BodyPart) (*model.BodyPart, error) {
	if err := r.db.Create(&bodyPart).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return bodyPart, nil
}

func (r *BodyPartRepo) UpdateField(bodyPart *model.BodyPart, field string, value interface{}) (*model.BodyPart, error) {
	if err := r.db.Model(&bodyPart).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return bodyPart, nil
}

func (r *BodyPartRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.BodyPart{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *BodyPartRepo) GetByID(id string) (*model.BodyPart, error) {
	bodyPart := &model.BodyPart{}

	if err := r.db.Where("id = ?", id).First(bodyPart).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return bodyPart, nil
}

func (r *BodyPartRepo) GetAll() ([]model.BodyPart, error) {
	bodyParts := []model.BodyPart{}

	if err := r.db.Find(&bodyParts).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return bodyParts, nil
}
