package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type ScheduleExerciseRepo struct {
	db *gorm.DB
}

func NewScheduleExerciseRepository(db *gorm.DB) model.ScheduleExerciseRepository {
	return &ScheduleExerciseRepo{db}
}

func (r *ScheduleExerciseRepo) Create(scheduleExercise *model.ScheduleExercise) (*model.ScheduleExercise, error) {
	if err := r.db.Create(&scheduleExercise).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return scheduleExercise, nil
}

func (r *ScheduleExerciseRepo) UpdateField(scheduleExercise *model.ScheduleExercise, field string, value interface{}) (*model.ScheduleExercise, error) {
	if err := r.db.Model(&scheduleExercise).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return scheduleExercise, nil
}

func (r *ScheduleExerciseRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.ScheduleExercise{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *ScheduleExerciseRepo) GetByID(id string) (*model.ScheduleExercise, error) {
	scheduleExercise := &model.ScheduleExercise{}

	if err := r.db.Where("id = ?", id).First(scheduleExercise).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return scheduleExercise, nil
}
