package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type ReportRepo struct {
	db *gorm.DB
}

func NewReportRepository(db *gorm.DB) model.ReportRepository {
	return &ReportRepo{db}
}

func (r *ReportRepo) Create(report *model.Report) (*model.Report, error) {
	if err := r.db.Create(&report).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return report, nil
}

func (r *ReportRepo) UpdateField(report *model.Report, field string, value interface{}) (*model.Report, error) {
	if err := r.db.Model(&report).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return report, nil
}

func (r *ReportRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.Report{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *ReportRepo) GetByID(id string) (*model.Report, error) {
	report := &model.Report{}

	if err := r.db.Where("id = ?", id).First(report).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return report, nil
}
