package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type ExerciseRepo struct {
	db *gorm.DB
}

func NewExerciseRepository(db *gorm.DB) model.ExerciseRepository {
	return &ExerciseRepo{db}
}

func (r *ExerciseRepo) Create(exercise *model.Exercise) (*model.Exercise, error) {
	if err := r.db.Create(&exercise).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return exercise, nil
}

func (r *ExerciseRepo) UpdateField(exercise *model.Exercise, field string, value interface{}) (*model.Exercise, error) {
	if err := r.db.Model(&exercise).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return exercise, nil
}

func (r *ExerciseRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.Exercise{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *ExerciseRepo) GetByID(id string) (*model.Exercise, error) {
	exercise := &model.Exercise{}

	if err := r.db.Where("id = ?", id).First(exercise).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return exercise, nil
}

func (r *ExerciseRepo) GetAll() ([]model.Exercise, error) {
	exercises := []model.Exercise{}

	if err := r.db.Find(&exercises).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return exercises, nil
}
