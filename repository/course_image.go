package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type CourseImageRepo struct {
	db *gorm.DB
}

func NewCourseImageRepository(db *gorm.DB) model.CourseImageRepository {
	return &CourseImageRepo{db}
}

func (r *CourseImageRepo) Create(courseImage *model.CourseImage) (*model.CourseImage, error) {
	if err := r.db.Create(&courseImage).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return courseImage, nil
}

func (r *CourseImageRepo) UpdateField(courseImage *model.CourseImage, field string, value interface{}) (*model.CourseImage, error) {
	if err := r.db.Model(&courseImage).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return courseImage, nil
}

func (r *CourseImageRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.CourseImage{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *CourseImageRepo) GetByID(id string) (*model.CourseImage, error) {
	courseImage := &model.CourseImage{}

	if err := r.db.Where("id = ?", id).First(courseImage).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return courseImage, nil
}
