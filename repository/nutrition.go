package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type NutritionRepo struct {
	db *gorm.DB
}

func NewNutritionRepository(db *gorm.DB) model.NutritionRepository {
	return &NutritionRepo{db}
}

func (r *NutritionRepo) Create(nutrition *model.Nutrition) (*model.Nutrition, error) {
	if err := r.db.Create(&nutrition).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return nutrition, nil
}

func (r *NutritionRepo) UpdateField(nutrition *model.Nutrition, field string, value interface{}) (*model.Nutrition, error) {
	if err := r.db.Model(&nutrition).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return nutrition, nil
}

func (r *NutritionRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.Nutrition{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *NutritionRepo) GetByID(id string) (*model.Nutrition, error) {
	nutrition := &model.Nutrition{}

	if err := r.db.Where("id = ?", id).First(nutrition).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return nutrition, nil
}

func (r *NutritionRepo) GetAll() ([]model.Nutrition, error) {
	nutritions := []model.Nutrition{}

	if err := r.db.Find(&nutritions).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return nutritions, nil
}
