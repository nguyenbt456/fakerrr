package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type TrainerRegistrationRepo struct {
	db *gorm.DB
}

func NewTrainerRegistrationRepository(db *gorm.DB) model.TrainerRegistrationRepository {
	return &TrainerRegistrationRepo{db}
}

func (r *TrainerRegistrationRepo) Create(trainerRegistration *model.TrainerRegistration) (*model.TrainerRegistration, error) {
	if err := r.db.Create(&trainerRegistration).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return trainerRegistration, nil
}

func (r *TrainerRegistrationRepo) UpdateField(trainerRegistration *model.TrainerRegistration, field string, value interface{}) (*model.TrainerRegistration, error) {
	if err := r.db.Model(&trainerRegistration).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return trainerRegistration, nil
}

func (r *TrainerRegistrationRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.TrainerRegistration{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *TrainerRegistrationRepo) GetByID(id string) (*model.TrainerRegistration, error) {
	trainerRegistration := &model.TrainerRegistration{}

	if err := r.db.Where("id = ?", id).First(trainerRegistration).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return trainerRegistration, nil
}
