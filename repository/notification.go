package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type NotificationRepo struct {
	db *gorm.DB
}

func NewNotificationRepository(db *gorm.DB) model.NotificationRepository {
	return &NotificationRepo{db}
}

func (r *NotificationRepo) Create(notification *model.Notification) (*model.Notification, error) {
	if err := r.db.Create(&notification).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return notification, nil
}

func (r *NotificationRepo) UpdateField(notification *model.Notification, field string, value interface{}) (*model.Notification, error) {
	if err := r.db.Model(&notification).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return notification, nil
}

func (r *NotificationRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.Notification{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *NotificationRepo) GetByID(id string) (*model.Notification, error) {
	notification := &model.Notification{}

	if err := r.db.Where("id = ?", id).First(notification).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return notification, nil
}
