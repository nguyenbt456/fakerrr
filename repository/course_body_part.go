package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type CourseBodyPartRepo struct {
	db *gorm.DB
}

func NewCourseBodyPartRepository(db *gorm.DB) model.CourseBodyPartRepository {
	return &CourseBodyPartRepo{db}
}

func (r *CourseBodyPartRepo) Create(cBodyPart *model.CourseBodyPart) (*model.CourseBodyPart, error) {
	if err := r.db.Create(&cBodyPart).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return cBodyPart, nil
}

func (r *CourseBodyPartRepo) UpdateField(cBodyPart *model.CourseBodyPart, field string, value interface{}) (*model.CourseBodyPart, error) {
	if err := r.db.Model(&cBodyPart).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return cBodyPart, nil
}

func (r *CourseBodyPartRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.CourseBodyPart{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *CourseBodyPartRepo) GetByID(id string) (*model.CourseBodyPart, error) {
	cBodyPart := &model.CourseBodyPart{}

	if err := r.db.Where("id = ?", id).First(cBodyPart).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return cBodyPart, nil
}
