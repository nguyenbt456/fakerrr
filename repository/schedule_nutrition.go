package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type ScheduleNutritionRepo struct {
	db *gorm.DB
}

func NewScheduleNutritionRepository(db *gorm.DB) model.ScheduleNutritionRepository {
	return &ScheduleNutritionRepo{db}
}

func (r *ScheduleNutritionRepo) Create(scheduleNutrition *model.ScheduleNutrition) (*model.ScheduleNutrition, error) {
	if err := r.db.Create(&scheduleNutrition).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return scheduleNutrition, nil
}

func (r *ScheduleNutritionRepo) UpdateField(scheduleNutrition *model.ScheduleNutrition, field string, value interface{}) (*model.ScheduleNutrition, error) {
	if err := r.db.Model(&scheduleNutrition).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return scheduleNutrition, nil
}

func (r *ScheduleNutritionRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.ScheduleNutrition{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *ScheduleNutritionRepo) GetByID(id string) (*model.ScheduleNutrition, error) {
	scheduleNutrition := &model.ScheduleNutrition{}

	if err := r.db.Where("id = ?", id).First(scheduleNutrition).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return scheduleNutrition, nil
}
