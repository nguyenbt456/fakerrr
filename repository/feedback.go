package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type FeedbackRepo struct {
	db *gorm.DB
}

func NewFeedbackRepository(db *gorm.DB) model.FeedbackRepository {
	return &FeedbackRepo{db}
}

func (r *FeedbackRepo) Create(feedback *model.Feedback) (*model.Feedback, error) {
	if err := r.db.Create(&feedback).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return feedback, nil
}

func (r *FeedbackRepo) UpdateField(feedback *model.Feedback, field string, value interface{}) (*model.Feedback, error) {
	if err := r.db.Model(&feedback).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return feedback, nil
}

func (r *FeedbackRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.Feedback{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *FeedbackRepo) GetByID(id string) (*model.Feedback, error) {
	feedback := &model.Feedback{}

	if err := r.db.Where("id = ?", id).First(feedback).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return feedback, nil
}
