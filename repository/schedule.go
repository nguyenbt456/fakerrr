package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type ScheduleRepo struct {
	db *gorm.DB
}

func NewScheduleRepository(db *gorm.DB) model.ScheduleRepository {
	return &ScheduleRepo{db}
}

func (r *ScheduleRepo) Create(schedule *model.Schedule) (*model.Schedule, error) {
	if err := r.db.Create(&schedule).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return schedule, nil
}

func (r *ScheduleRepo) UpdateField(schedule *model.Schedule, field string, value interface{}) (*model.Schedule, error) {
	if err := r.db.Model(&schedule).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return schedule, nil
}

func (r *ScheduleRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.Schedule{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *ScheduleRepo) GetByID(id string) (*model.Schedule, error) {
	schedule := &model.Schedule{}

	if err := r.db.Where("id = ?", id).First(schedule).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return schedule, nil
}

func (r *ScheduleRepo) GetAll() ([]model.Schedule, error) {
	schedules := []model.Schedule{}

	if err := r.db.Find(&schedules).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return schedules, nil
}
