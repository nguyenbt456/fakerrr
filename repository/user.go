package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type UserRepo struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) model.UserRepository {
	return &UserRepo{db}
}

func (r *UserRepo) Create(user *model.User) (*model.User, error) {
	if err := r.db.Create(&user).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return user, nil
}

func (r *UserRepo) UpdateField(user *model.User, field string, value interface{}) (*model.User, error) {
	if err := r.db.Model(&user).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return user, nil
}

func (r *UserRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.User{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *UserRepo) GetByID(id string) (*model.User, error) {
	user := &model.User{}

	if err := r.db.Where("id = ?", id).First(user).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return user, nil
}

func (r *UserRepo) GetByUsername(username string) (*model.User, error) {
	user := &model.User{}

	if err := r.db.Where("username = ?", username).First(user).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return user, nil
}

func (r *UserRepo) GetByType(userType int) ([]model.User, error) {
	users := []model.User{}

	if err := r.db.Where("type = ?", userType).Find(&users).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return users, nil
}
