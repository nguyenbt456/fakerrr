package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type CourseFoodRepo struct {
	db *gorm.DB
}

func NewCourseFoodRepository(db *gorm.DB) model.CourseFoodRepository {
	return &CourseFoodRepo{db}
}

func (r *CourseFoodRepo) Create(courseFood *model.CourseFood) (*model.CourseFood, error) {
	if err := r.db.Create(&courseFood).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return courseFood, nil
}

func (r *CourseFoodRepo) UpdateField(courseFood *model.CourseFood, field string, value interface{}) (*model.CourseFood, error) {
	if err := r.db.Model(&courseFood).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return courseFood, nil
}

func (r *CourseFoodRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.CourseFood{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *CourseFoodRepo) GetByID(id string) (*model.CourseFood, error) {
	courseFood := &model.CourseFood{}

	if err := r.db.Where("id = ?", id).First(courseFood).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return courseFood, nil
}
