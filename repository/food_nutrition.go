package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type FoodNutritionRepo struct {
	db *gorm.DB
}

func NewFoodNutritionRepository(db *gorm.DB) model.FoodNutritionRepository {
	return &FoodNutritionRepo{db}
}

func (r *FoodNutritionRepo) Create(foodNutrition *model.FoodNutrition) (*model.FoodNutrition, error) {
	if err := r.db.Create(&foodNutrition).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return foodNutrition, nil
}

func (r *FoodNutritionRepo) UpdateField(foodNutrition *model.FoodNutrition, field string, value interface{}) (*model.FoodNutrition, error) {
	if err := r.db.Model(&foodNutrition).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return foodNutrition, nil
}

func (r *FoodNutritionRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.FoodNutrition{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *FoodNutritionRepo) GetByID(id string) (*model.FoodNutrition, error) {
	foodNutrition := &model.FoodNutrition{}

	if err := r.db.Where("id = ?", id).First(foodNutrition).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return foodNutrition, nil
}
