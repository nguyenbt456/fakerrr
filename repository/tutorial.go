package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type TutorialRepo struct {
	db *gorm.DB
}

func NewTutorialRepository(db *gorm.DB) model.TutorialRepository {
	return &TutorialRepo{db}
}

func (r *TutorialRepo) Create(tutorial *model.Tutorial) (*model.Tutorial, error) {
	if err := r.db.Create(&tutorial).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return tutorial, nil
}

func (r *TutorialRepo) UpdateField(tutorial *model.Tutorial, field string, value interface{}) (*model.Tutorial, error) {
	if err := r.db.Model(&tutorial).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return tutorial, nil
}

func (r *TutorialRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.Tutorial{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *TutorialRepo) GetByID(id string) (*model.Tutorial, error) {
	tutorial := &model.Tutorial{}

	if err := r.db.Where("id = ?", id).First(tutorial).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return tutorial, nil
}
