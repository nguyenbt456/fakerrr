package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type ExerciseBodyPartRepo struct {
	db *gorm.DB
}

func NewExerciseBodyPartRepository(db *gorm.DB) model.ExerciseBodyPartRepository {
	return &ExerciseBodyPartRepo{db}
}

func (r *ExerciseBodyPartRepo) Create(eBodyPart *model.ExerciseBodyPart) (*model.ExerciseBodyPart, error) {
	if err := r.db.Create(&eBodyPart).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return eBodyPart, nil
}

func (r *ExerciseBodyPartRepo) UpdateField(eBodyPart *model.ExerciseBodyPart, field string, value interface{}) (*model.ExerciseBodyPart, error) {
	if err := r.db.Model(&eBodyPart).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return eBodyPart, nil
}

func (r *ExerciseBodyPartRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.ExerciseBodyPart{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *ExerciseBodyPartRepo) GetByID(id string) (*model.ExerciseBodyPart, error) {
	eBodyPart := &model.ExerciseBodyPart{}

	if err := r.db.Where("id = ?", id).First(eBodyPart).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return eBodyPart, nil
}
