package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type CourseCategoryRepo struct {
	db *gorm.DB
}

func NewCourseCategoryRepository(db *gorm.DB) model.CourseCategoryRepository {
	return &CourseCategoryRepo{db}
}

func (r *CourseCategoryRepo) Create(courseCategory *model.CourseCategory) (*model.CourseCategory, error) {
	if err := r.db.Create(&courseCategory).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return courseCategory, nil
}

func (r *CourseCategoryRepo) UpdateField(courseCategory *model.CourseCategory, field string, value interface{}) (*model.CourseCategory, error) {
	if err := r.db.Model(&courseCategory).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return courseCategory, nil
}

func (r *CourseCategoryRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.CourseCategory{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *CourseCategoryRepo) GetByID(id string) (*model.CourseCategory, error) {
	courseCategory := &model.CourseCategory{}

	if err := r.db.Where("id = ?", id).First(courseCategory).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return courseCategory, nil
}
