package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type NutritionVitaminRepository struct {
	db *gorm.DB
}

func NewNutritionVitaminRepository(db *gorm.DB) model.NutritionVitaminRepository {
	return &NutritionVitaminRepository{db}
}

func (r *NutritionVitaminRepository) Create(nutritionVitamin *model.NutritionVitamin) (*model.NutritionVitamin, error) {
	if err := r.db.Create(&nutritionVitamin).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return nutritionVitamin, nil
}

func (r *NutritionVitaminRepository) UpdateField(nutritionVitamin *model.NutritionVitamin, field string, value interface{}) (*model.NutritionVitamin, error) {
	if err := r.db.Model(&nutritionVitamin).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return nutritionVitamin, nil
}

func (r *NutritionVitaminRepository) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.NutritionVitamin{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *NutritionVitaminRepository) GetByID(id string) (*model.NutritionVitamin, error) {
	nutritionVitamin := &model.NutritionVitamin{}

	if err := r.db.Where("id = ?", id).First(nutritionVitamin).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return nutritionVitamin, nil
}

func (r *NutritionVitaminRepository) GetAll() ([]model.NutritionVitamin, error) {
	nutritionVitamins := []model.NutritionVitamin{}

	if err := r.db.Find(&nutritionVitamins).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return nutritionVitamins, nil
}
