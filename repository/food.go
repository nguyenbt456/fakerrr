package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type FoodRepo struct {
	db *gorm.DB
}

func NewFoodRepository(db *gorm.DB) model.FoodRepository {
	return &FoodRepo{db}
}

func (r *FoodRepo) Create(food *model.Food) (*model.Food, error) {
	if err := r.db.Create(&food).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return food, nil
}

func (r *FoodRepo) UpdateField(food *model.Food, field string, value interface{}) (*model.Food, error) {
	if err := r.db.Model(&food).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return food, nil
}

func (r *FoodRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.Food{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *FoodRepo) GetByID(id string) (*model.Food, error) {
	food := &model.Food{}

	if err := r.db.Where("id = ?", id).First(food).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return food, nil
}

func (r *FoodRepo) GetAll() ([]model.Food, error) {
	foods := []model.Food{}

	if err := r.db.Find(&foods).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return foods, nil
}
