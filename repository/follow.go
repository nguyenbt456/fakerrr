package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type FollowRepo struct {
	db *gorm.DB
}

func NewFollowRepository(db *gorm.DB) model.FollowRepository {
	return &FollowRepo{db}
}

func (r *FollowRepo) Create(follow *model.Follow) (*model.Follow, error) {
	if err := r.db.Create(&follow).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return follow, nil
}

func (r *FollowRepo) UpdateField(follow *model.Follow, field string, value interface{}) (*model.Follow, error) {
	if err := r.db.Model(&follow).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return follow, nil
}

func (r *FollowRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.Follow{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *FollowRepo) GetByID(id string) (*model.Follow, error) {
	follow := &model.Follow{}

	if err := r.db.Where("id = ?", id).First(follow).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return follow, nil
}
