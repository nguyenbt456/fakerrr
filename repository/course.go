package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type CourseRepo struct {
	db *gorm.DB
}

func NewCourseRepository(db *gorm.DB) model.CourseRepository {
	return &CourseRepo{db}
}

func (r *CourseRepo) Create(course *model.Course) (*model.Course, error) {
	if err := r.db.Create(&course).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return course, nil
}

func (r *CourseRepo) UpdateField(course *model.Course, field string, value interface{}) (*model.Course, error) {
	if err := r.db.Model(&course).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return course, nil
}

func (r *CourseRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.Course{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *CourseRepo) GetByID(id string) (*model.Course, error) {
	course := &model.Course{}

	if err := r.db.Where("id = ?", id).First(course).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return course, nil
}

func (r *CourseRepo) GetAll() ([]model.Course, error) {
	courses := []model.Course{}

	if err := r.db.Find(&courses).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return courses, nil
}
