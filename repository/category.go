package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type CategoryRepo struct {
	db *gorm.DB
}

func NewCategoryRepository(db *gorm.DB) model.CategoryRepository {
	return &CategoryRepo{db}
}

func (r *CategoryRepo) Create(category *model.Category) (*model.Category, error) {
	if err := r.db.Create(&category).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return category, nil
}

func (r *CategoryRepo) UpdateField(category *model.Category, field string, value interface{}) (*model.Category, error) {
	if err := r.db.Model(&category).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return category, nil
}

func (r *CategoryRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.Category{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *CategoryRepo) GetByID(id string) (*model.Category, error) {
	category := &model.Category{}

	if err := r.db.Where("id = ?", id).First(category).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return category, nil
}

func (r *CategoryRepo) GetAll() ([]model.Category, error) {
	categories := []model.Category{}

	if err := r.db.Find(&categories).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return categories, nil
}
