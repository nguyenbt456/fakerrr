package repository

import (
	"errors"

	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

type NutritionChemicalCompositionRepo struct {
	db *gorm.DB
}

func NewNutritionChemicalCompositionRepository(db *gorm.DB) model.NutritionChemicalCompositionRepository {
	return &NutritionChemicalCompositionRepo{db}
}

func (r *NutritionChemicalCompositionRepo) Create(nutritionCC *model.NutritionChemicalComposition) (*model.NutritionChemicalComposition, error) {
	if err := r.db.Create(&nutritionCC).Error; err != nil {
		return nil, errors.New("Bad Gateway")
	}
	return nutritionCC, nil
}

func (r *NutritionChemicalCompositionRepo) UpdateField(nutritionCC *model.NutritionChemicalComposition, field string, value interface{}) (*model.NutritionChemicalComposition, error) {
	if err := r.db.Model(&nutritionCC).Update(field, value).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, err
	}
	return nutritionCC, nil
}

func (r *NutritionChemicalCompositionRepo) DeleteByID(id string) error {
	if err := r.db.Where("id = ?", id).Delete(&model.NutritionChemicalComposition{}).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return errors.New("Record Not Found")
		}
		return errors.New("Bad Gateway")
	}
	return nil
}

func (r *NutritionChemicalCompositionRepo) GetByID(id string) (*model.NutritionChemicalComposition, error) {
	nutritionCC := &model.NutritionChemicalComposition{}

	if err := r.db.Where("id = ?", id).First(nutritionCC).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return nutritionCC, nil
}

func (r *NutritionChemicalCompositionRepo) GetAll() ([]model.NutritionChemicalComposition, error) {
	nutritionCCs := []model.NutritionChemicalComposition{}

	if err := r.db.Find(&nutritionCCs).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return nil, errors.New("Record Not Found")
		}
		return nil, errors.New("Bad Gateway")
	}
	return nutritionCCs, nil
}
