package main

import (
	"log"
	"os"
	"strconv"

	"gitlab.com/lotusfitness/fakerrr/controller"
	"gitlab.com/lotusfitness/fakerrr/database"
)

func main() {
	database.ConnectDB()

	amountStr := os.Getenv("amount")
	amount, _ := strconv.Atoi(amountStr)
	if amount == 0 {
		log.Println(amount)
		log.Println("amount can not be null")
		return
	}

	controller.FakeData(amount)
}
