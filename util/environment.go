package util

import (
	"os"
	"strconv"
)

func getEVString(key string, defaultValue string) string {
	value := os.Getenv(key)

	if len(value) == 0 {
		return defaultValue
	}

	return value
}

func getEVInt64(key string, defaultValue int64) int64 {
	value := os.Getenv(key)

	if len(value) == 0 {
		return defaultValue
	}

	result, err := strconv.ParseInt(value, 10, 64)
	if err != nil {
		return defaultValue
	}

	return result
}

// EVString get environment variable by string type
func EVString(key string, defaultValue string) string {
	return getEVString(key, defaultValue)
}

// EVInt get environment variable by int type
func EVInt(key string, defaultValue int) int {
	return int(getEVInt64(key, int64(defaultValue)))
}

// EVInt64 get environment variable by int64 type
func EVInt64(key string, defaultValue int64) int64 {
	return getEVInt64(key, defaultValue)
}
