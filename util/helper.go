package util

import (
	"log"
	"math/rand"
	"time"

	"golang.org/x/crypto/bcrypt"
)

// GeneratePassword convert password to hash
func GeneratePassword(password string) (string, error) {
	saltedBytes := []byte(password)
	hashedBytes, err := bcrypt.GenerateFromPassword(saltedBytes, bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	hash := string(hashedBytes[:])
	return hash, nil
}

//ComparePassword compare password with hash
func ComparePassword(hash string, password string) bool {
	incoming := []byte(password)
	existing := []byte(hash)
	err := bcrypt.CompareHashAndPassword(existing, incoming)
	if err != nil {
		log.Println("Fail to compare password: ", err)
		return false
	}
	return true
}

// RandomTime ...
func RandomTime() time.Time {
	randomTime := rand.Int63n(time.Now().Unix()-94608000) + 94608000

	return time.Unix(randomTime, 0)
}
