package controller

import (
	"log"

	"gitlab.com/lotusfitness/fakerrr/database"
	"gitlab.com/lotusfitness/fakerrr/service"
)

// FakeData ...
func FakeData(amount int) {
	db := database.GetDB()

	userService := service.NewUserService(db)
	userService.Fake(amount)

	exerciseService := service.NewExerciseService(db)
	exerciseService.Fake(amount)

	nutritionService := service.NewNutritionService(db)
	nutritionService.Fake(amount)

	foodService := service.NewFoodService(db)
	foodService.Fake(amount)

	courseService := service.NewCourseService(db)
	courseService.Fake(amount)

	feedbackService := service.NewFeedbackService(db)
	feedbackService.Fake(amount)

	followService := service.NewFollowService(db)
	followService.Fake(amount)

	notificationService := service.NewNotificationService(db)
	notificationService.Fake(amount)

	reportService := service.NewReportService(db)
	reportService.Fake(amount)

	// success
	log.Println("Success")
}
