package database

import (
	"fmt"

	"gitlab.com/lotusfitness/fakerrr/util"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

const (
	postgresHost     = "POSTGRES_HOST"
	postgresPort     = "POSTGRES_PORT"
	postgresUser     = "POSTGRES_USER"
	postgresPassword = "POSTGRES_PASSWORD"
	postgresDBName   = "POSTGRES_DB"
	postgresSSLMode  = "POSTGRES_SSLMODE"

	defaultPgHost     = "127.0.0.1"
	defaultPgPort     = "5432"
	defaultPgUser     = "lf"
	defaultPgPassword = "123456"
	defaultPgDBName   = "lotusfitness"
	defaultPgSSLMode  = "disable"
)

type postgresConfig struct {
	Host     string
	Port     string
	User     string
	Password string
	DBName   string
	SSLMode  string
}

var db *gorm.DB

// GetDB return db
func GetDB() *gorm.DB {
	return db
}

// ConnectDB create connection to postgresDB
func ConnectDB() {
	cfg := postgresConfig{
		Host:     util.EVString(postgresHost, defaultPgHost),
		Port:     util.EVString(postgresPort, defaultPgPort),
		User:     util.EVString(postgresUser, defaultPgUser),
		Password: util.EVString(postgresPassword, defaultPgPassword),
		DBName:   util.EVString(postgresDBName, defaultPgDBName),
		SSLMode:  util.EVString(postgresSSLMode, defaultPgSSLMode),
	}

	dsn := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
		cfg.Host, cfg.Port, cfg.User, cfg.Password, cfg.DBName, cfg.SSLMode)

	var err error
	if db, err = gorm.Open(postgres.New(postgres.Config{DSN: dsn}), nil); err != nil {
		panic(err)
	}
}
