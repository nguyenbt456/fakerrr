package model

// CourseCategory represents for course_categories table
type CourseCategory struct {
	ID         string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('course_categories_id_seq')" faker:"-"`
	CourseID   string `json:"course_id" gorm:"type:bigint" faker:"-"`
	CategoryID string `json:"category_id" gorm:"type:bigint" faker:"-"`
}

// CourseCategoryRepository ...
type CourseCategoryRepository interface {
	Create(*CourseCategory) (*CourseCategory, error)
	UpdateField(courseCategory *CourseCategory, field string, value interface{}) (*CourseCategory, error)
	DeleteByID(string) error
	GetByID(string) (*CourseCategory, error)
}
