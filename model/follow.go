package model

// Follow represents for follows table
type Follow struct {
	ID         string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('follows_id_seq')" faker:"-"`
	UserID     string `json:"user_id" gorm:"type:bigint" faker:"-"`
	CategoryID string `json:"category_id" gorm:"type:bigint" faker:"-"`
}

// FollowRepository ...
type FollowRepository interface {
	Create(*Follow) (*Follow, error)
	UpdateField(follow *Follow, field string, value interface{}) (*Follow, error)
	DeleteByID(string) error
	GetByID(string) (*Follow, error)
}
