package model

// CourseFood represents for course_foods table
type CourseFood struct {
	ID       string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('course_foods_id_seq')" faker:"-"`
	CourseID string `json:"course_id" gorm:"type:bigint" faker:"-"`
	FoodID   string `json:"food_id" gorm:"type:bigint" faker:"-"`
}

// CourseFoodRepository ...
type CourseFoodRepository interface {
	Create(*CourseFood) (*CourseFood, error)
	UpdateField(courseFood *CourseFood, field string, value interface{}) (*CourseFood, error)
	DeleteByID(string) error
	GetByID(string) (*CourseFood, error)
}
