package model

// NutritionChemicalComposition represents for nutrition_chemical_compositions table
type NutritionChemicalComposition struct {
	ID          string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('nutrition_chemical_compositions_id_seq')" faker:"-"`
	NutritionID string `json:"nutrition_id" gorm:"type:bigint" faker:"-"`
	Name        string `json:"name"`
	Amount      int    `json:"amount"`
	Unit        string `json:"unit"`
}

// NutritionChemicalCompositionRepository ...
type NutritionChemicalCompositionRepository interface {
	Create(*NutritionChemicalComposition) (*NutritionChemicalComposition, error)
	UpdateField(nutritionChemicalComposition *NutritionChemicalComposition, field string, value interface{}) (*NutritionChemicalComposition, error)
	DeleteByID(string) error
	GetByID(string) (*NutritionChemicalComposition, error)
	GetAll() ([]NutritionChemicalComposition, error)
}
