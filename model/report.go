package model

// Report represents for reports table
type Report struct {
	ID             string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('reports_id_seq')" faker:"-"`
	UserID         string `json:"user_id" gorm:"type:bigint" faker:"-"`
	ReportedUserID string `json:"reported_user_id" gorm:"type:bigint" faker:"-"`
	Content        string `json:"content"`
}

// ReportRepository ...
type ReportRepository interface {
	Create(*Report) (*Report, error)
	UpdateField(report *Report, field string, value interface{}) (*Report, error)
	DeleteByID(string) error
	GetByID(string) (*Report, error)
}
