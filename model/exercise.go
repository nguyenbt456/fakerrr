package model

// Exercise represents for exercises table
type Exercise struct {
	ID               string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('exercises_id_seq')" faker:"-"`
	Name             string `json:"name"`
	Description      string `json:"description"`
	Duration         int    `json:"duration"`
	Contraindication string `json:"contraindication"`
	Avatar           string `json:"avatar" faker:"-"`
	Video            string `json:"video" faker:"-"`
}

// ExerciseRepository ...
type ExerciseRepository interface {
	Create(*Exercise) (*Exercise, error)
	UpdateField(exercise *Exercise, field string, value interface{}) (*Exercise, error)
	DeleteByID(string) error
	GetByID(string) (*Exercise, error)
	GetAll() ([]Exercise, error)
}
