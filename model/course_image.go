package model

// CourseImage represents for course_images table
type CourseImage struct {
	ID       string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('course_images_id_seq')" faker:"-"`
	CourseID string `json:"course_id" gorm:"type:bigint" faker:"-"`
	Image    string `json:"image" faker:"-"`
}

// CourseImageRepository ...
type CourseImageRepository interface {
	Create(*CourseImage) (*CourseImage, error)
	UpdateField(courseImage *CourseImage, field string, value interface{}) (*CourseImage, error)
	DeleteByID(string) error
	GetByID(string) (*CourseImage, error)
}
