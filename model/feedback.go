package model

// Feedback represents for feedbacks table
type Feedback struct {
	ID         string  `json:"id" gorm:"primaryKey;type:bigint;default:next_id('feedbacks_id_seq')" faker:"-"`
	UserID     string  `json:"user_id" gorm:"type:bigint;default:null" faker:"-"`
	CourseID   *string `json:"course_id" gorm:"type:bigint;default:null" faker:"-"`
	ScheduleID *string `json:"schedule_id" gorm:"type:bigint;default:null" faker:"-"`
}

// FeedbackRepository ...
type FeedbackRepository interface {
	Create(*Feedback) (*Feedback, error)
	UpdateField(feedback *Feedback, field string, value interface{}) (*Feedback, error)
	DeleteByID(string) error
	GetByID(string) (*Feedback, error)
}
