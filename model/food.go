package model

// Food represents for foods table
type Food struct {
	ID          string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('foods_id_seq')" faker:"-"`
	Name        string `json:"name"`
	Avatar      string `json:"avatar" faker:"-"`
	Description string `json:"description"`
	Amount      int    `json:"amount"`
	Unit        string `json:"unit"`
}

// FoodRepository ...
type FoodRepository interface {
	Create(*Food) (*Food, error)
	UpdateField(food *Food, field string, value interface{}) (*Food, error)
	DeleteByID(string) error
	GetByID(string) (*Food, error)
	GetAll() ([]Food, error)
}

// TableName ...
func (f *Food) TableName() string {
	return "foods"
}
