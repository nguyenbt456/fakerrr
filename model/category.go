package model

// Category represents for categories table
type Category struct {
	ID     string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('categories_id_seq')" faker:"-"`
	Name   string `json:"name"`
	Avatar string `json:"avatar"`
}

// CategoryRepository ...
type CategoryRepository interface {
	Create(*Category) (*Category, error)
	UpdateField(category *Category, field string, value interface{}) (*Category, error)
	DeleteByID(string) error
	GetByID(string) (*Category, error)
	GetAll() ([]Category, error)
}
