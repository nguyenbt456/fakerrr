package model

// Notification represents for notifications table
type Notification struct {
	ID         string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('notifications_id_seq')" faker:"-"`
	Title      string `json:"title"`
	Content    string `json:"content"`
	ReceiverID string `json:"receiver_id" gorm:"type:bigint" faker:"-"`
	SenderID   string `json:"sender_id" gorm:"type:bigint" faker:"-"`
}

// NotificationRepository ...
type NotificationRepository interface {
	Create(*Notification) (*Notification, error)
	UpdateField(notification *Notification, field string, value interface{}) (*Notification, error)
	DeleteByID(string) error
	GetByID(string) (*Notification, error)
}
