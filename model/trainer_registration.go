package model

// TrainerRegistration represents for trainer_registrations table
type TrainerRegistration struct {
	ID              string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('trainer_registrations_id_seq')" faker:"-"`
	UserID          string `json:"user_id" gorm:"type:bigint" faker:"-"`
	CertificateLink string `json:"certificate_link" faker:"-"`
}

// TrainerRegistrationRepository ...
type TrainerRegistrationRepository interface {
	Create(*TrainerRegistration) (*TrainerRegistration, error)
	UpdateField(trainerRegistration *TrainerRegistration, field string, value interface{}) (*TrainerRegistration, error)
	DeleteByID(string) error
	GetByID(string) (*TrainerRegistration, error)
}
