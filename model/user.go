package model

import (
	"time"
)

// User represents for users table
type User struct {
	ID          string     `json:"id" gorm:"primaryKey;type:bigint;default:next_id('users_id_seq')" faker:"-"`
	Username    string     `json:"username,omitempty"`
	Password    string     `json:"-" faker:"-"`
	Email       string     `json:"email,omitempty"`
	Fullname    string     `json:"fullname,omitempty"`
	PhoneNumber string     `json:"phone_number,omitempty"`
	Address     string     `json:"address,omitempty"`
	Avatar      string     `json:"avatar,omitempty"`
	Type        int        `json:"type" faker:"-"`
	Height      int        `json:"height,omitempty"`
	Weight      int        `json:"weight,omitempty"`
	Birthday    *time.Time `json:"birthday,omitempty"`
}

// UserRepository ...
type UserRepository interface {
	Create(*User) (*User, error)
	UpdateField(user *User, field string, value interface{}) (*User, error)
	DeleteByID(string) error
	GetByID(string) (*User, error)
	GetByUsername(string) (*User, error)
	GetByType(userType int) ([]User, error)
}

// UserService ...
type UserService interface{}
