package model

// ExerciseBodyPart represents for exercise_body_parts table
type ExerciseBodyPart struct {
	ID         string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('exercise_body_parts_id_seq')" faker:"-"`
	ExerciseID string `json:"exercise_id" gorm:"type:bigint" faker:"-"`
	BodyPartID string `json:"body_part_id" gorm:"type:bigint" faker:"-"`
}

// ExerciseBodyPartRepository ...
type ExerciseBodyPartRepository interface {
	Create(*ExerciseBodyPart) (*ExerciseBodyPart, error)
	UpdateField(exerciseBodyPart *ExerciseBodyPart, field string, value interface{}) (*ExerciseBodyPart, error)
	DeleteByID(string) error
	GetByID(string) (*ExerciseBodyPart, error)
}
