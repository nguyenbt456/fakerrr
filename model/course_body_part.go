package model

// CourseBodyPart represents for course_body_parts table
type CourseBodyPart struct {
	ID         string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('course_body_parts_id_seq')" faker:"-"`
	CourseID   string `json:"course_id" gorm:"type:bigint" faker:"-"`
	BodyPartID string `json:"body_part_id" gorm:"type:bigint" faker:"-"`

	// foreign_key
	Course   Course   `json:"-" gorm:"foreignKey:course_id;references:id"`
	BodyPart BodyPart `json:"-" gorm:"foreignKey:body_part_id;references:id"`
}

// CourseBodyPartRepository ...
type CourseBodyPartRepository interface {
	Create(*CourseBodyPart) (*CourseBodyPart, error)
	UpdateField(courseBodyPart *CourseBodyPart, field string, value interface{}) (*CourseBodyPart, error)
	DeleteByID(string) error
	GetByID(string) (*CourseBodyPart, error)
}
