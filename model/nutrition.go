package model

// Nutrition represents for nutritions table
type Nutrition struct {
	ID          string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('nutritions_id_seq')" faker:"-"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Energy      string `json:"energy"`
	Avatar      string `json:"avatar" faker:"-"`
	Amount      int    `json:"amount"`
	Unit        string `json:"unit"`
}

// NutritionRepository ...
type NutritionRepository interface {
	Create(*Nutrition) (*Nutrition, error)
	UpdateField(nutrition *Nutrition, field string, value interface{}) (*Nutrition, error)
	DeleteByID(string) error
	GetByID(string) (*Nutrition, error)
	GetAll() ([]Nutrition, error)
}
