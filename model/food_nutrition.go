package model

// FoodNutrition represents for food_nutritions table
type FoodNutrition struct {
	ID          string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('food_nutritions_id_seq')" faker:"-"`
	FoodID      string `json:"food_id" gorm:"type:bigint" faker:"-"`
	NutritionID string `json:"nutrition_id" gorm:"type:bigint" faker:"-"`
}

// FoodNutritionRepository ...
type FoodNutritionRepository interface {
	Create(*FoodNutrition) (*FoodNutrition, error)
	UpdateField(foodNutrition *FoodNutrition, field string, value interface{}) (*FoodNutrition, error)
	DeleteByID(string) error
	GetByID(string) (*FoodNutrition, error)
}
