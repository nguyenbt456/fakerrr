package model

// Tutorial represents for toturials table
type Tutorial struct {
	ID         string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('toturials_id_seq')" faker:"-"`
	Content    string `json:"content"`
	Priority   int    `json:"priority" faker:"-"`
	ExerciseID string `json:"exercise_id" gorm:"type:bigint" faker:"-"`
}

// TutorialRepository ...
type TutorialRepository interface {
	Create(*Tutorial) (*Tutorial, error)
	UpdateField(tutorial *Tutorial, field string, value interface{}) (*Tutorial, error)
	DeleteByID(string) error
	GetByID(string) (*Tutorial, error)
}
