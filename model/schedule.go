package model

import "time"

// Schedule represents for schedules table
type Schedule struct {
	ID       string    `json:"id" gorm:"primaryKey;type:bigint;default:next_id('schedules_id_seq')" faker:"-"`
	CourseID string    `json:"course_id" gorm:"type:bigint" faker:"-"`
	UserID   string    `json:"user_id" faker:"-"`
	AtTime   time.Time `json:"at_time"`
}

// ScheduleRepository ...
type ScheduleRepository interface {
	Create(*Schedule) (*Schedule, error)
	UpdateField(schedule *Schedule, field string, value interface{}) (*Schedule, error)
	DeleteByID(string) error
	GetByID(string) (*Schedule, error)
	GetAll() ([]Schedule, error)
}
