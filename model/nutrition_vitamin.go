package model

// NutritionVitamin represents for nutrition_vitamins table
type NutritionVitamin struct {
	ID          string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('nutrition_vitamins_id_seq')" faker:"-"`
	NutritionID string `json:"nutrition_id" gorm:"type:bigint" faker:"-"`
	Vitamin     string `json:"vitamin"`
}

// NutritionVitaminRepository ...
type NutritionVitaminRepository interface {
	Create(*NutritionVitamin) (*NutritionVitamin, error)
	UpdateField(nutritionVitamin *NutritionVitamin, field string, value interface{}) (*NutritionVitamin, error)
	DeleteByID(string) error
	GetByID(string) (*NutritionVitamin, error)
	GetAll() ([]NutritionVitamin, error)
}
