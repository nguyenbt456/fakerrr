package model

// ScheduleNutrition represents for schedule_nutritions table
type ScheduleNutrition struct {
	ID          string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('schedule_nutritions_id_seq')" faker:"-"`
	ScheduleID  string `json:"schedule_id" gorm:"type:bigint" faker:"-"`
	NutritionID string `json:"nutrition_id" gorm:"type:bigint" faker:"-"`
}

// ScheduleNutritionRepository ...
type ScheduleNutritionRepository interface {
	Create(*ScheduleNutrition) (*ScheduleNutrition, error)
	UpdateField(scheduleNutrition *ScheduleNutrition, field string, value interface{}) (*ScheduleNutrition, error)
	DeleteByID(string) error
	GetByID(string) (*ScheduleNutrition, error)
}
