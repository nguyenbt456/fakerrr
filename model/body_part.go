package model

// BodyPart represents for body_parts table
type BodyPart struct {
	ID     string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('body_parts_id_seq')" faker:"-"`
	Name   string `json:"name"`
	Avatar string `json:"avatar"`
}

// BodyPartRepository ...
type BodyPartRepository interface {
	Create(*BodyPart) (*BodyPart, error)
	UpdateField(bodyPart *BodyPart, field string, value interface{}) (*BodyPart, error)
	DeleteByID(string) error
	GetByID(string) (*BodyPart, error)
	GetAll() ([]BodyPart, error)
}
