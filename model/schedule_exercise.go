package model

// ScheduleExercise represents for schedule_exercises table
type ScheduleExercise struct {
	ID         string `json:"id" gorm:"primaryKey;type:bigint;default:next_id('schedule_exercises_id_seq')" faker:"-"`
	ScheduleID string `json:"schedule_id" gorm:"type:bigint" faker:"-"`
	ExerciseID string `json:"exercise_id" gorm:"type:bigint" faker:"-"`
}

// ScheduleExerciseRepository ...
type ScheduleExerciseRepository interface {
	Create(*ScheduleExercise) (*ScheduleExercise, error)
	UpdateField(scheduleExercise *ScheduleExercise, field string, value interface{}) (*ScheduleExercise, error)
	DeleteByID(string) error
	GetByID(string) (*ScheduleExercise, error)
}
