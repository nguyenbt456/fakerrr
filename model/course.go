package model

import "time"

// Course represents for courses table
type Course struct {
	ID          string    `json:"id" gorm:"primaryKey;type:bigint;default:next_id('courses_id_seq')" faker:"-"`
	Name        string    `json:"name"`
	Description string    `json:"description"`
	Duration    int       `json:"duration"`
	Address     string    `json:"address"`
	StartTime   time.Time `json:"start_time" faker:"-"`
	EndTime     time.Time `json:"end_time" faker:"-"`
	Fee         int       `json:"fee"`
	MaxStudent  int       `json:"max_student"`
	UserID      string    `json:"user_id" gorm:"type:bigint" faker:"-"`
	Status      int       `json:"status"` // waiting:0 | accepted: 1 | rejected:2 | blocked:3
}

// CourseRepository ...
type CourseRepository interface {
	Create(*Course) (*Course, error)
	UpdateField(course *Course, field string, value interface{}) (*Course, error)
	DeleteByID(string) error
	GetByID(string) (*Course, error)
	GetAll() ([]Course, error)
}
