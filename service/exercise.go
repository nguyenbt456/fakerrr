package service

import (
	"math/rand"

	"github.com/bxcodec/faker"
	"gitlab.com/lotusfitness/fakerrr/model"
	"gitlab.com/lotusfitness/fakerrr/repository"
	"gorm.io/gorm"
)

// ExerciseService ...
type ExerciseService struct {
	repo                 model.ExerciseRepository
	bodyPartRepo         model.BodyPartRepository
	exerciseBodyPartRepo model.ExerciseBodyPartRepository
	tutorialRepo         model.TutorialRepository
}

func NewExerciseService(db *gorm.DB) *ExerciseService {
	repo := repository.NewExerciseRepository(db)
	bodyPartRepo := repository.NewBodyPartRepository(db)
	exerciseBodyPartRepo := repository.NewExerciseBodyPartRepository(db)
	tutorialRepo := repository.NewTutorialRepository(db)

	return &ExerciseService{
		repo:                 repo,
		bodyPartRepo:         bodyPartRepo,
		exerciseBodyPartRepo: exerciseBodyPartRepo,
		tutorialRepo:         tutorialRepo,
	}
}

// Fake ...
func (s *ExerciseService) Fake(num int) []model.Exercise {
	values := []model.Exercise{}

	bodyParts, _ := s.bodyPartRepo.GetAll()
	for i := 0; i < num; i++ {
		value := &model.Exercise{
			Avatar: "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
			Video:  "https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4",
		}
		faker.FakeData(value)

		value, _ = s.repo.Create(value)
		values = append(values, *value)

		// create 5 exercise_body_part
		for j := 0; j < 5; j++ {
			bodyPartIndex := rand.Intn(len(bodyParts))
			exerciseBodyPart := &model.ExerciseBodyPart{
				ExerciseID: value.ID,
				BodyPartID: bodyParts[bodyPartIndex].ID,
			}

			s.exerciseBodyPartRepo.Create(exerciseBodyPart)
		}

		// create 5 tutorial
		for j := 0; j < 5; j++ {
			tutorial := &model.Tutorial{
				Priority:   j,
				ExerciseID: value.ID,
			}
			faker.FakeData(value)

			s.tutorialRepo.Create(tutorial)
		}
	}

	return values
}
