package service

import (
	"math/rand"
	"time"

	"gitlab.com/lotusfitness/fakerrr/repository"

	"github.com/bxcodec/faker"
	"gitlab.com/lotusfitness/fakerrr/model"
	"gorm.io/gorm"
)

// CourseService ...
type CourseService struct {
	repo                  model.CourseRepository
	bodyPartRepo          model.BodyPartRepository
	categoryRepo          model.CategoryRepository
	courseCategoryRepo    model.CourseCategoryRepository
	courseBodyPartRepo    model.CourseBodyPartRepository
	courseImageRepo       model.CourseImageRepository
	courseFoodRepo        model.CourseFoodRepository
	foodRepo              model.FoodRepository
	scheduleRepo          model.ScheduleRepository
	scheduleExerciseRepo  model.ScheduleExerciseRepository
	exerciseRepo          model.ExerciseRepository
	scheduleNutritionRepo model.ScheduleNutritionRepository
	nutritionRepo         model.NutritionRepository
	userRepo              model.UserRepository
}

func NewCourseService(db *gorm.DB) *CourseService {
	repo := repository.NewCourseRepository(db)
	bodyPartRepo := repository.NewBodyPartRepository(db)
	categoryRepo := repository.NewCategoryRepository(db)
	courseCategoryRepo := repository.NewCourseCategoryRepository(db)
	courseBodyPartRepo := repository.NewCourseBodyPartRepository(db)
	courseImageRepo := repository.NewCourseImageRepository(db)
	courseFoodRepo := repository.NewCourseFoodRepository(db)
	foodRepo := repository.NewFoodRepository(db)
	scheduleRepo := repository.NewScheduleRepository(db)
	scheduleExerciseRepo := repository.NewScheduleExerciseRepository(db)
	exerciseRepo := repository.NewExerciseRepository(db)
	scheduleNutritionRepo := repository.NewScheduleNutritionRepository(db)
	nutritionRepo := repository.NewNutritionRepository(db)
	userRepo := repository.NewUserRepository(db)

	return &CourseService{
		repo:                  repo,
		bodyPartRepo:          bodyPartRepo,
		categoryRepo:          categoryRepo,
		courseCategoryRepo:    courseCategoryRepo,
		courseBodyPartRepo:    courseBodyPartRepo,
		courseImageRepo:       courseImageRepo,
		courseFoodRepo:        courseFoodRepo,
		foodRepo:              foodRepo,
		scheduleRepo:          scheduleRepo,
		scheduleExerciseRepo:  scheduleExerciseRepo,
		exerciseRepo:          exerciseRepo,
		scheduleNutritionRepo: scheduleNutritionRepo,
		nutritionRepo:         nutritionRepo,
		userRepo:              userRepo,
	}

}

// Fake ...
func (s *CourseService) Fake(num int) []model.Course {
	values := []model.Course{}

	users, _ := s.userRepo.GetByType(1)
	bodyParts, _ := s.bodyPartRepo.GetAll()
	categories, _ := s.categoryRepo.GetAll()
	foods, _ := s.foodRepo.GetAll()
	exercises, _ := s.exerciseRepo.GetAll()
	nutritions, _ := s.nutritionRepo.GetAll()
	for i := 0; i < num; i++ {
		userIndex := rand.Intn(len(users))
		categoryIndex := rand.Intn(len(categories))

		value := &model.Course{
			StartTime: time.Now().AddDate(0, 0, i),
			EndTime:   time.Now().AddDate(0, 0, i+10),
			UserID:    users[userIndex].ID,
		}
		faker.FakeData(value)

		value, _ = s.repo.Create(value)
		values = append(values, *value)

		// create course_categories
		courseCategory := &model.CourseCategory{
			CourseID:   value.ID,
			CategoryID: categories[categoryIndex].ID,
		}
		s.courseCategoryRepo.Create(courseCategory)

		// create 5 body_part
		for j := 0; j < 5; j++ {
			bodyPartIndex := rand.Intn(len(bodyParts))
			courseBodyPart := &model.CourseBodyPart{
				CourseID:   value.ID,
				BodyPartID: bodyParts[bodyPartIndex].ID,
			}

			s.courseBodyPartRepo.Create(courseBodyPart)
		}

		// create 5 course_image
		for j := 0; j < 5; j++ {
			courseImage := &model.CourseImage{
				CourseID: value.ID,
				Image:    "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
			}

			s.courseImageRepo.Create(courseImage)
		}

		// create 10 course_food
		for j := 0; j < 10; j++ {
			foodIndex := rand.Intn(len(foods))
			courseFood := &model.CourseFood{
				CourseID: value.ID,
				FoodID:   foods[foodIndex].ID,
			}

			s.courseFoodRepo.Create(courseFood)
		}

		// create 10 schedule
		for j := 0; j < 10; j++ {
			schedule := &model.Schedule{
				CourseID: value.ID,
				UserID:   users[userIndex].ID,
				AtTime:   time.Now().AddDate(0, 0, j),
			}

			schedule, _ = s.scheduleRepo.Create(schedule)

			// create 5 schedule_exercise
			for k := 0; k < 5; k++ {
				exerciseIndex := rand.Intn(len(exercises))
				scheduleExercise := &model.ScheduleExercise{
					ScheduleID: schedule.ID,
					ExerciseID: exercises[exerciseIndex].ID,
				}

				s.scheduleExerciseRepo.Create(scheduleExercise)
			}

			// create 5 schedule_nutrition
			for k := 0; k < 5; k++ {
				nutritionIndex := rand.Intn(len(nutritions))
				scheduleNutrition := &model.ScheduleNutrition{
					ScheduleID:  schedule.ID,
					NutritionID: nutritions[nutritionIndex].ID,
				}

				s.scheduleNutritionRepo.Create(scheduleNutrition)
			}

		}
	}

	return values
}
