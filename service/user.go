package service

import (
	"github.com/bxcodec/faker"
	"gitlab.com/lotusfitness/fakerrr/model"
	"gitlab.com/lotusfitness/fakerrr/repository"
	"gitlab.com/lotusfitness/fakerrr/util"
	"gorm.io/gorm"
)

// UserService ...
type UserService struct {
	repo        model.UserRepository
	trainerRepo model.TrainerRegistrationRepository
}

func NewUserService(db *gorm.DB) *UserService {
	repo := repository.NewUserRepository(db)
	trainerRepo := repository.NewTrainerRegistrationRepository(db)

	return &UserService{
		repo:        repo,
		trainerRepo: trainerRepo,
	}
}

// Fake ...
func (s *UserService) Fake(num int) []model.User {
	values := []model.User{}

	for i := 0; i < num; i++ {
		value := &model.User{}
		faker.FakeData(value)

		value.Password, _ = util.GeneratePassword("123456")
		value.Type = i % 2

		value, _ = s.repo.Create(value)
		values = append(values, *value)

		// create trainer registration
		if value.Type == 1 {
			s.trainerRepo.Create(&model.TrainerRegistration{
				UserID:          value.ID,
				CertificateLink: "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
			})
		}
	}

	return values
}
