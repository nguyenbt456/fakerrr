package service

import (
	"math/rand"

	"github.com/bxcodec/faker"
	"gitlab.com/lotusfitness/fakerrr/model"
	"gitlab.com/lotusfitness/fakerrr/repository"
	"gorm.io/gorm"
)

// FoodService ...
type FoodService struct {
	repo              model.FoodRepository
	foodNutritionRepo model.FoodNutritionRepository
	nutritionRepo     model.NutritionRepository
}

func NewFoodService(db *gorm.DB) *FoodService {
	repo := repository.NewFoodRepository(db)
	foodNutritionRepo := repository.NewFoodNutritionRepository(db)
	nutritionRepo := repository.NewNutritionRepository(db)

	return &FoodService{
		repo:              repo,
		foodNutritionRepo: foodNutritionRepo,
		nutritionRepo:     nutritionRepo,
	}
}

// Fake ...
func (s *FoodService) Fake(num int) []model.Food {
	values := []model.Food{}

	nutritions, _ := s.nutritionRepo.GetAll()
	for i := 0; i < num; i++ {
		value := &model.Food{
			Avatar: "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
		}
		faker.FakeData(value)

		value, _ = s.repo.Create(value)
		values = append(values, *value)

		// create 5 food_nutrition
		for j := 0; j < 5; j++ {
			nutritionIndex := rand.Intn(len(nutritions))
			foodNutrition := &model.FoodNutrition{
				FoodID:      value.ID,
				NutritionID: nutritions[nutritionIndex].ID,
			}

			s.foodNutritionRepo.Create(foodNutrition)
		}
	}

	return values
}
