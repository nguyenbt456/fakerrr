package service

import (
	"math/rand"

	"github.com/bxcodec/faker"
	"gitlab.com/lotusfitness/fakerrr/model"
	"gitlab.com/lotusfitness/fakerrr/repository"
	"gorm.io/gorm"
)

// FollowService ...
type FollowService struct {
	repo         model.FollowRepository
	userRepo     model.UserRepository
	categoryRepo model.CategoryRepository
}

func NewFollowService(db *gorm.DB) *FollowService {
	repo := repository.NewFollowRepository(db)
	userRepo := repository.NewUserRepository(db)
	categoryRepo := repository.NewCategoryRepository(db)

	return &FollowService{
		repo:         repo,
		userRepo:     userRepo,
		categoryRepo: categoryRepo,
	}
}

// Fake ...
func (s *FollowService) Fake(num int) []model.Follow {
	values := []model.Follow{}

	users, _ := s.userRepo.GetByType(0)
	categories, _ := s.categoryRepo.GetAll()
	for i := 0; i < num; i++ {
		userIndex := rand.Intn(len(users))
		categoryIndex := rand.Intn(len(categories))

		value := &model.Follow{
			UserID:     users[userIndex].ID,
			CategoryID: categories[categoryIndex].ID,
		}
		faker.FakeData(value)

		value, _ = s.repo.Create(value)
		values = append(values, *value)
	}

	return values
}
