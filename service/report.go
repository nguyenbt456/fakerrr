package service

import (
	"math/rand"

	"github.com/bxcodec/faker"
	"gitlab.com/lotusfitness/fakerrr/model"
	"gitlab.com/lotusfitness/fakerrr/repository"
	"gorm.io/gorm"
)

// ReportService ...
type ReportService struct {
	repo     model.ReportRepository
	userRepo model.UserRepository
}

func NewReportService(db *gorm.DB) *ReportService {
	repo := repository.NewReportRepository(db)
	userRepo := repository.NewUserRepository(db)

	return &ReportService{
		repo:     repo,
		userRepo: userRepo,
	}
}

// Fake ...
func (s *ReportService) Fake(num int) []model.Report {
	values := []model.Report{}

	users, _ := s.userRepo.GetByType(0)
	for i := 0; i < num; i++ {
		userIndex := rand.Intn(len(users) - 1)
		value := &model.Report{
			UserID:         users[userIndex].ID,
			ReportedUserID: users[userIndex].ID,
		}
		faker.FakeData(value)

		value, _ = s.repo.Create(value)
		values = append(values, *value)
	}

	return values
}
