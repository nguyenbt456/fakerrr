package service

import (
	"github.com/bxcodec/faker"
	"gitlab.com/lotusfitness/fakerrr/model"
	"gitlab.com/lotusfitness/fakerrr/repository"
	"gorm.io/gorm"
)

// NutritionService ...
type NutritionService struct {
	repo                 model.NutritionRepository
	nutritionVitaminrepo model.NutritionVitaminRepository
	nutritionCCrepo      model.NutritionChemicalCompositionRepository
}

func NewNutritionService(db *gorm.DB) *NutritionService {
	repo := repository.NewNutritionRepository(db)
	nutritionVitaminrepo := repository.NewNutritionVitaminRepository(db)
	nutritionCCrepo := repository.NewNutritionChemicalCompositionRepository(db)

	return &NutritionService{
		repo:                 repo,
		nutritionVitaminrepo: nutritionVitaminrepo,
		nutritionCCrepo:      nutritionCCrepo,
	}
}

// Fake ...
func (s *NutritionService) Fake(num int) []model.Nutrition {
	values := []model.Nutrition{}

	for i := 0; i < num; i++ {
		value := &model.Nutrition{
			Avatar: "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
		}
		faker.FakeData(value)

		value, _ = s.repo.Create(value)
		values = append(values, *value)

		// create 5 nutrition_vitamin
		for j := 0; j < 5; j++ {
			nutritionVitamin := &model.NutritionVitamin{
				NutritionID: value.ID,
			}
			faker.FakeData(nutritionVitamin)

			s.nutritionVitaminrepo.Create(nutritionVitamin)
		}

		// create 5 nutrition_chemical_composition
		for j := 0; j < 5; j++ {
			nutritionCC := &model.NutritionChemicalComposition{
				NutritionID: value.ID,
			}
			faker.FakeData(nutritionCC)

			s.nutritionCCrepo.Create(nutritionCC)
		}
	}

	return values
}
