package service

import (
	"math/rand"

	"github.com/bxcodec/faker"
	"gitlab.com/lotusfitness/fakerrr/model"
	"gitlab.com/lotusfitness/fakerrr/repository"
	"gorm.io/gorm"
)

// NotificationService ...
type NotificationService struct {
	repo     model.NotificationRepository
	userRepo model.UserRepository
}

func NewNotificationService(db *gorm.DB) *NotificationService {
	repo := repository.NewNotificationRepository(db)
	userRepo := repository.NewUserRepository(db)

	return &NotificationService{
		repo:     repo,
		userRepo: userRepo,
	}
}

// Fake ...
func (s *NotificationService) Fake(num int) []model.Notification {
	values := []model.Notification{}

	users, _ := s.userRepo.GetByType(0)
	trainers, _ := s.userRepo.GetByType(1)
	for i := 0; i < num; i++ {
		userIndex := rand.Intn(len(users))
		trainerIndex := rand.Intn(len(trainers))
		value := &model.Notification{
			SenderID:   trainers[trainerIndex].ID,
			ReceiverID: users[userIndex].ID,
		}
		faker.FakeData(value)

		value, _ = s.repo.Create(value)
		values = append(values, *value)
	}

	return values
}
