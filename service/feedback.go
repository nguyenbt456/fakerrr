package service

import (
	"math/rand"

	"github.com/bxcodec/faker"
	"gitlab.com/lotusfitness/fakerrr/model"
	"gitlab.com/lotusfitness/fakerrr/repository"
	"gorm.io/gorm"
)

// FeedbackService ...
type FeedbackService struct {
	repo         model.FeedbackRepository
	userRepo     model.UserRepository
	courseRepo   model.CourseRepository
	scheduleRepo model.ScheduleRepository
}

func NewFeedbackService(db *gorm.DB) *FeedbackService {
	repo := repository.NewFeedbackRepository(db)
	userRepo := repository.NewUserRepository(db)
	courseRepo := repository.NewCourseRepository(db)
	scheduleRepo := repository.NewScheduleRepository(db)

	return &FeedbackService{
		repo:         repo,
		userRepo:     userRepo,
		courseRepo:   courseRepo,
		scheduleRepo: scheduleRepo,
	}
}

// Fake ...
func (s *FeedbackService) Fake(num int) []model.Feedback {
	values := []model.Feedback{}

	users, _ := s.userRepo.GetByType(0)
	courses, _ := s.courseRepo.GetAll()
	schedules, _ := s.scheduleRepo.GetAll()
	for i := 0; i < num; i++ {
		userIndex := rand.Intn(len(users))
		courseIndex := rand.Intn(len(courses))
		scheduleIndex := rand.Intn(len(schedules))

		value := &model.Feedback{
			UserID: users[userIndex].ID,
		}

		if i%2 == 0 {
			value.CourseID = &(courses[courseIndex].ID)
		} else {
			value.ScheduleID = &(schedules[scheduleIndex].ID)
		}
		faker.FakeData(value)

		value, _ = s.repo.Create(value)
		values = append(values, *value)
	}

	return values
}
